**Description of Problem:**

Based on data obtained from each engine cycle and from additional settings predict the failure of an engine on the next cycle. 
The max cycle of an engine - is the last cycle before crash. Make a proposal how to use this model in practice. Construct an alternative problem statement.

**Data:**

Initial data - Excel-file with 16.138 engine cycles.

**Columns:**

* id - engine id
* cycle - no. of cycle
* p00-p20 - values of sensors during cycle
* s0, s1 - settings correspond to definite cycle


Each cycle is characterized by 23 values (21 'p' - averaged values from sensors during the cycle and 2's' - settings during the current cycle). 
The last cycle of engine is the last cycle before failure.

**Project Files:**

* data.xlsx - containes given data
* crashAnalysis.ipynb - Jupyter Notebook with analysis and model constructing
